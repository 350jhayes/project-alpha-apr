from django.shortcuts import render
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def project_list(request):
    bannana = Project.objects.filter(owner=request.user)
    context = {
        "project_list": bannana,
    }
    return render(request, "list.html", context)


@login_required
def show_projects(request, id):
    projects_tasks = Task.objects.filter(id=id)
    context = {
        "show_tasks": projects_tasks,
    }
    return render(request, "projects/detail.html", context)
